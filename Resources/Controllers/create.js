function init(win)
{
	var TiTools = require("TiTools/TiTools");

	//---------------------------------------------//
	
	var controller = TiTools.UI.Loader.load('Views/Create.js', win);
	
	//---------------------------------------------//
	
	var photo_blob,
		thumb_photo_blob;
	
	var user = Ti.App.Parseapi.PFUserCurrentUser();	
		
	//---------------------------------------------//
	
	var photoBtn = controller.photoBtn;
	photoBtn.addEventListener('singletap', function()
	{
		// open the user's gallery, and let the user choose a photo to upload
		Titanium.Media.openPhotoGallery(
		{
			mediaTypes: [Ti.Media.MEDIA_TYPE_PHOTO],
			error: function(event) 
			{
				Ti.UI.createAlertDialog({
			        message: "Can't take photo from gallery",
			        title: "Error"
		        }).show();
			},
			success:function(event)
			{	
				photo_blob = event.media;
				thumb_photo_blob = photo_blob.imageAsThumbnail(106, 0, 8)
						
				var thumbPhotoImageView = TiTools.UI.Controls.createImageView({
					image: thumb_photo_blob,
					height: Ti.UI.FILL,
					width: Ti.UI.FILL,
					hires: true,
					margin: 9
				});
				
				photoBtn.add(thumbPhotoImageView);	
			}
		});
	});
	
	//---------------------------------------------//
	
	controller.createBtn.addEventListener("singletap", function()
	{
		var name 	= controller.nameField.getValue(),
		 	mark 	= controller.markField.getValue(),
		 	place	= controller.placeField.getValue(),
		 	comment = controller.commentField.getValue();
		
		// validate
		if(!name || !mark || !photo_blob)
		{
			Ti.UI.createAlertDialog({
		        message: "Name, Mark and Photo can not be empty!",
		        title: "Error"
	        }).show();
			return false;			
		}	
		
		Ti.App.fireEvent("showIndicator");
		
		// create parse files
		var file = Ti.App.Parseapi.PFFileCreateFileWithData(thumb_photo_blob);
			
		// new good object	
		var good = Ti.App.Parseapi.PFObjectCreateObjectWithClassName("Good");	
		
		// save file
		file.saveInBackground(
		{
			success: function()
			{
				try
				{
					// now save this file in a good object
					good.setObject(file, "photo");
					// other info
					good.setObject(name, "name");
					good.setObject(mark, "mark");
					good.setObject(place, "place");
					good.setObject(comment, "comment");
					good.setObject(user.objectId, "userId");
					// save object async
					good.saveInBackground(
						{
							success: function()
							{
								Ti.App.fireEvent("hideIndicator");
								win.close();
								win.table.appendRow(
									TiTools.UI.Controls.createTableViewRow(
										{ 
											title: name,
											good: good
										}
									)
								);
							},
							error: function()
							{
								Ti.App.fireEvent("hideIndicator");	
							}
						}
					);
				}
				catch(e)
				{
					Ti.App.fireEvent("hideIndicator");
					Ti.UI.createAlertDialog({
				        message: e.message,
				        title: "Error"
			        }).show();			        
				}
			},
			error: function()
			{
				Ti.App.fireEvent("hideIndicator");
				Ti.UI.createAlertDialog({
			        message: "Can't save file!",
			        title: "Error"
		        }).show();	  
		        return false;
			}
		});		
		
	});
}

// controller
module.exports = init;