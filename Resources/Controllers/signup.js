function init(win)
{
	var TiTools = require("TiTools/TiTools");

	//---------------------------------------------//
	
	var controller = TiTools.UI.Loader.load('Views/Signup.js', win);
	
	//---------------------------------------------//
	
	// sign up event handler
	controller.signUpBtn.addEventListener('singletap', function()
	{
		var surname 		= controller.surnameField.getValue(),
		 	name 	   		= controller.nameField.getValue(),
		 	email 	   		= controller.emailField.getValue(),
		 	password	    = controller.passwordField.getValue(),
		 	confirmPassword = controller.confirmPasswordField.getValue();
		
		// all fields can not be empty
		if(!surname || !name || !email || !password || !confirmPassword)
		{
			Ti.UI.createAlertDialog({
		        message: "All fields are required!",
		        title: "Error"
	        }).show();
			return false;			
		}
		
		// validate email 
		var regExpEmail = new RegExp('^([a-z0-9_-]+\.)*[a-z0-9_-]+@[a-z0-9_-]+(\.[a-z0-9_-]+)*\.[a-z]{2,4}$', 'i');
		if(!regExpEmail.test(email))
		{
			Ti.UI.createAlertDialog({
		        message: "Invalid email!",
		        title: "Error"
	        }).show();
	        return false;
		}
		
		// confirm password
		if(password.toString() !== confirmPassword.toString())
		{
			Ti.UI.createAlertDialog({
		        message: "Passwords do not match!",
		        title: "Error"
	        }).show();
			return false;		
		}
		
		Ti.App.fireEvent('showIndicator');
		
		// --- REGISTRATION --- //			
		var pfUser = Ti.App.Parseapi.createPFUser();   // first create a pfUser object
		
		pfUser.initSignUp({
	        username: email.toLowerCase(),
	        password: password,
	        email: null 
	    });
		
		pfUser.setObject(surname, "surname");
		pfUser.setObject(name, "name");
		
		var result = pfUser.finishSignUpInBackground(
		{
			success: function()
			{
				var goodsWin = TiTools.UI.Controls.createWindow({
					title: "Goods",
					main: 'Controllers/main.js'
				});			
				
				Ti.App.Properties.setBool('login', true);
				TiTools.Utils.getCurrentTab().activeTab.open(goodsWin, {animated: true});	
				Ti.App.fireEvent('hideIndicator');
			},
			error: function(e)
			{
	        	Ti.UI.createAlertDialog({
			        message: "Sign up failed!",
			        title: "Error"
		        }).show();
		        Ti.App.fireEvent('hideIndicator');
			}
		});
	});
}

// controller
module.exports = init;