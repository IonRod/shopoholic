function init(win)
{
	var TiTools = require("TiTools/TiTools");

	//---------------------------------------------//
	
	var controller = TiTools.UI.Loader.load('Views/Main.js', win);
	
	//---------------------------------------------//
	
	var table = controller.me,
		user  = Ti.App.Parseapi.PFUserCurrentUser();
		
	var search = Titanium.UI.createSearchBar({
	    barColor: '#385292', 
	    showCancel: false,
	    height: 43,
	    top: 0,
	});
	
	table.search = search;
	
	//---------------------------------------------//
	
	/*
	var createBtnWin = TiTools.UI.Controls.createWindow(
		{
			height: 40,
			bottom: 0,
			backgroundColor: "#d3d3d3"
		}
	);
	*/
	
	var createBtn = TiTools.UI.Controls.createButton(
		{
			title: "Create"
		}
	);
	
	createBtn.addEventListener("click", function()
		{
			var createGoodWin = TiTools.UI.Controls.createWindow({
				title: "Create good",
				table: table,
				main: 'Controllers/create.js'
			});			
			
			TiTools.Utils.getCurrentTab().activeTab.open(createGoodWin, {animated: true});
		}
	);
	
	//createBtnWin.add(createBtn);
	//win.add(createBtnWin);
	
	win.rightNavButton = createBtn;
	
	//---------------------------------------------//
	
	var logoutBtn = TiTools.UI.Controls.createButton(
		{
			title: "Log out"
		}
	);
	
	logoutBtn.addEventListener("click", function()
		{
		
			Ti.App.Properties.setBool('login', false);
			
			// create tab group and show login screen
			var tabGroup = TiTools.UI.Controls.createTabGroup({
				bottom: -50
			});		
			
			var index = TiTools.UI.Controls.createWindow({
				title: "Shopoholic",
				main: "Controllers/index.js"
			});
			
			tabGroup.addTab(
				Ti.UI.createTab({
					parent: tabGroup,
					window: index	
				})
			);
			
			win.close();
			TiTools.Utils.getCurrentTab().close();
			
			TiTools.currentTab = tabGroup;
			
			tabGroup.open({
				transition: Ti.UI.iPhone && Ti.UI.iPhone.AnimationStyle.FLIP_FROM_LEFT
			});	
		}
	);
	
	win.leftNavButton = logoutBtn;
	
	//---------------------------------------------//
	
	// create table view event listener
	table.addEventListener('click', function(e)
	{
		var row = e.row;
		if(row.good)
		{
			var viewGoodWin = TiTools.UI.Controls.createWindow({
				title: row.title,
				main: 'Controllers/view.js',
				good: row.good
			});			
			
			TiTools.Utils.getCurrentTab().activeTab.open(viewGoodWin, {animated: true});		
		}
	});
	
	//---------------------------------------------//
	
	// load goods
	var query = Ti.App.Parseapi.PFQueryCreateQueryWithClassName("Good");
	
	query.whereKey({
	    key: "userId",
	    equalTo: user.objectId   // can be equalTo, greaterThan, greaterThanOrEqualTo, lessThan, lessThanOrEqualTo, notEqualTo
	});
	
	query.findObjectsInBackground(
		{
			success: function(e)
			{
				var goods = e.objects;
				for(var key in goods)
				{
					var goodObj = goods[key];
					table.appendRow(
						TiTools.UI.Controls.createTableViewRow(
							{ 
								title: goodObj.objectForKey("name"),
								good: goodObj
							}
						)
					);
				}		
			},
			error: function()
			{
				
			}
		}
	);
}
// controller
module.exports = init;