function init(win)
{
	var TiTools = require("TiTools/TiTools");

	//---------------------------------------------//
	
	var controller = TiTools.UI.Loader.load('Views/Index.js', win);
	
	//---------------------------------------------//
	
	// log in process handler
	controller.logInBtn.addEventListener("singletap", function()
	{
		var email    = controller.emailField.getValue(),
		    password = controller.passwordField.getValue();
		
		// check
		if(!email || !password)
		{
			Ti.UI.createAlertDialog({
		        message: "Email and Password field can not be empty!",
		        title: "Error"
	        }).show();	 
	        return false;
		}		
		
		Ti.App.fireEvent('showIndicator');	
					
		// --- LOG IN --- //	
		var result = Ti.App.Parseapi.PFUserLoginInBackground({
		    username: email.toLowerCase(),
		    password: password,
		    success: function()
		    {
		    	var goodsWin = TiTools.UI.Controls.createWindow({
					title: "Goods",
					main: 'Controllers/main.js'
				});			
				
				Ti.App.Properties.setBool('login', true);
				
				TiTools.Utils.getCurrentTab().activeTab.open(goodsWin, {animated: true});	
				Ti.App.fireEvent('hideIndicator');
		    },
		    error: function(e)
		    {
		    	Ti.UI.createAlertDialog({
			        message: "Log in failed! Check data for correct!",
			        title: "Error"
		        }).show();
		        Ti.App.fireEvent('hideIndicator');
		    }
	    });
	});
	
	//---------------------------------------------//
	
	// show sign up window
	controller.signUpBtn.addEventListener("singletap", function()
	{
		var signUpWin = TiTools.UI.Controls.createWindow({
			title: "Sign Up",
			main: 'Controllers/signup.js'
		});			
		
		TiTools.Utils.getCurrentTab().activeTab.open(signUpWin, {animated: true});		
	});
}

// controller
module.exports = init;