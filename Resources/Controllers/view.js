function init(win)
{
	var TiTools = require("TiTools/TiTools");

	//---------------------------------------------//
	
	var controller = TiTools.UI.Loader.load('Views/Create.js', win);
	
	//---------------------------------------------//
	
	var good = win.good;
	
	//---------------------------------------------//
	
	controller.nameField.value      = good.objectForKey("name");
	controller.nameField.enabled    = false;
	
	controller.markField.value      = good.objectForKey("mark");
	controller.markField.enabled    = false;
	
	controller.placeField.value     = good.objectForKey("place");
	controller.placeField.enabled   = false;
	
	controller.commentField.value   = good.objectForKey("comment");
	controller.commentField.enabled = false;
	
	controller.createBtn.hide();
	
	//---------------------------------------------//
	
	var photo = good.objectForKey("photo");
	
	if(photo)
	{
		var photoPanel = controller.photoBtn;
		var thumbActivityIndicator = Ti.UI.createActivityIndicator({
			style: Ti.UI.iPhone && Ti.UI.iPhone.ActivityIndicatorStyle.DARK
		});
		
		photoPanel.add(thumbActivityIndicator);
		thumbActivityIndicator.show();
		
		// get file's data in background
		photo.getDataInBackground(
		{
			success: function(e)
			{
				var thumbImageView = TiTools.UI.Controls.createImageView({
					image: e.data,
					height: 106,
					width: 106,
					hires: true,
					borderRadius: 8,
					margin: 9
				});
				
				// remove activity indicator
				photoPanel.remove(thumbActivityIndicator);
				thumbActivityIndicator = null;
				
				// add thumbnail
				photoPanel.add(thumbImageView);
			}
		});
	}
}

// controller
module.exports = init;