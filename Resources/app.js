
// require TiTools
var TiTools = require('TiTools/TiTools');
	TiTools.initLibraries(); 

//---------------------------------------------//

Ti.UI.setBackgroundColor('#f2f2f2');

//---------------------------------------------//

// load parse module
Ti.App.Parseapi = require('com.forge42.parseapi');
Ti.App.Parseapi.enableParseModuleDebugLog(true);

Ti.App.Parseapi.initParse({
	applicationId: "1BaAD14RAsxnvEdryMVdZXYqAGvx3qFD88Hcv8El",
	clientKey: "tOg06GaAAEWSVwAYXqsABrdphmH2K6S4gfKCkoau",
	facebookApplicationId: ""
});

//---------------------------------------------//

//  CREATE CUSTOM LOADING INDICATOR
var indWin = null;
function showIndicator(message)
{
	// window container
	indWin = Ti.UI.createWindow();

	// black view
	var indView = Ti.UI.createView({
		height: Ti.UI.SIZE,
		width: Ti.Platform.displayCaps.platformWidth - 50,
		backgroundColor: '#000',
		borderRadius: 10,
		opacity: 0.8,
		layout: "vertical"
	});
	
	// loading indicator	
	var actInd = Ti.UI.createActivityIndicator({
		style: Ti.UI.iPhone && Ti.UI.iPhone.ActivityIndicatorStyle.BIG,
		top: 25,
		height: 30,
		width: 30
	});
	indView.add(actInd);

	// message			
	indView.add(Ti.UI.createLabel({
		text: message,
		color: '#fff',
		font: {fontSize: 20, fontWeight: 'bold'},
		top: 25
	}));
	
	// empty view
	indView.add(Ti.UI.createView({
		height: 20	
	}));
	
	// add black container view
	indWin.add(indView);
	
	// close event handler
	indWin.addEventListener('close', function()
	{
		indView = null;
		actInd  = null;
	});
	
	indWin.open();	
	actInd.show();
}

function hideIndicator()
{
	indWin.close({
		opacity: 0,
		duration: 500
	});
	indWin = null;
}

// Add global event handlers to hide/show custom indicator
Titanium.App.addEventListener('showIndicator', function(e)
{	
	if(!Ti.App.indicator)
	{
		Ti.App.indicator = true;
		var message = e.message || 'Wait...';
		showIndicator(message);
	}
});

Titanium.App.addEventListener('hideIndicator', function(e)
{
	if(Ti.App.indicator)
	{
		Ti.App.indicator = false;
		hideIndicator();
	}
});

//---------------------------------------------//

// create tab group and show login screen
var tabGroup = TiTools.UI.Controls.createTabGroup({
	bottom: -50
});		

var user = Ti.App.Parseapi.PFUserCurrentUser();
// index window
if(user === null || !Ti.App.Properties.getBool('login'))
{
	var index = TiTools.UI.Controls.createWindow({
		title: "Shopoholic",
		main: "Controllers/index.js"
	});
}
else
{
	var index = TiTools.UI.Controls.createWindow({
		title: "Goods",
		main: "Controllers/main.js"
	});	
}

tabGroup.addTab(
	Ti.UI.createTab({
		parent: tabGroup,
		window: index	
	})
);

TiTools.currentTab = tabGroup;

tabGroup.open({
	transition: Ti.UI.iPhone && Ti.UI.iPhone.AnimationStyle.FLIP_FROM_LEFT
});	


