exports = {
	outlet: "me",
	style: {
		className: "Ti.UI.ScrollView",
		layout: "vertical"
	},
	subviews: [
		{
			outlet: "surnameField",
			style: {
				className: "Ti.UI.TextField",
				borderStyle: Ti.UI.INPUT_BORDERSTYLE_ROUNDED,
  				color: '#336699',
  				hintText: "Surname",
  				top: 10, 
  				left: 10,
  				right: 10,
  				width: Ti.UI.FILL,
  				height: 40
			}
		},
		{
			outlet: "nameField",
			style: {
				className: "Ti.UI.TextField",
				borderStyle: Ti.UI.INPUT_BORDERSTYLE_ROUNDED,
  				color: '#336699',
  				hintText: "Name",
  				top: 10, 
  				left: 10,
  				right: 10,
  				width: Ti.UI.FILL,
  				height: 40
			}
		},
		{
			outlet: "emailField",
			style: {
				className: "Ti.UI.TextField",
				borderStyle: Ti.UI.INPUT_BORDERSTYLE_ROUNDED,
  				color: '#336699',
  				hintText: "Email",
  				top: 10, 
  				left: 10,
  				right: 10,
  				width: Ti.UI.FILL,
  				height: 40
			}
		},
		{
			outlet: "passwordField",
			style: {
				className: "Ti.UI.TextField",
				borderStyle: Ti.UI.INPUT_BORDERSTYLE_ROUNDED,
  				color: '#336699',
  				hintText: "Password",
  				passwordMask: true,
  				top: 10, 
  				left: 10,
  				right: 10,
  				width: Ti.UI.FILL,
  				height: 40
			}
		},	
		{
			outlet: "confirmPasswordField",
			style: {
				className: "Ti.UI.TextField",
				borderStyle: Ti.UI.INPUT_BORDERSTYLE_ROUNDED,
  				color: '#336699',
  				hintText: "Confirm password",
  				passwordMask: true,
  				top: 10, 
  				left: 10,
  				right: 10,
  				width: Ti.UI.FILL,
  				height: 40
			}
		},	
		{
			outlet: "signUpBtn",
			style: {
				className: "Ti.UI.Button",
				systemButton: Ti.UI.iPhone.SystemButton.DONE,				
				title: "Sign up",
				top: 45,
				left: 30,
				right: 30,
				width: Ti.UI.FILL,
				height: 40
			}
		}
	]
}