exports = {
	outlet: "me",
	style: {
		className: "Ti.UI.View",
		layout: "vertical"
	},
	subviews: [
		{
			outlet: "emailField",
			style: {
				className: "Ti.UI.TextField",
				borderStyle: Ti.UI.INPUT_BORDERSTYLE_ROUNDED,
  				color: '#336699',
  				hintText: "Email",
  				top: 10, 
  				left: 10,
  				right: 10,
  				width: Ti.UI.FILL,
  				height: 40
			}
		},
		{
			outlet: "passwordField",
			style: {
				className: "Ti.UI.TextField",
				borderStyle: Ti.UI.INPUT_BORDERSTYLE_ROUNDED,
  				color: '#336699',
  				hintText: "Password",
  				passwordMask: true,
  				top: 10, 
  				left: 10,
  				right: 10,
  				width: Ti.UI.FILL,
  				height: 40
			}
		},
		{
			outlet: "logInBtn",
			style: {
				className: "Ti.UI.Button",
				systemButton: Ti.UI.iPhone.SystemButton.DONE,				
				title: "Log in",
				top: 40,
				left: 30,
				right: 30,
				width: Ti.UI.FILL,
				height: 40
			}
		},
		{
			outlet: "signUpBtn",
			style: {
				className: "Ti.UI.Button",
				systemButton: Ti.UI.iPhone.SystemButton.DONE,				
				title: "Sign up",
				top: 15,
				left: 30,
				right: 30,
				width: Ti.UI.FILL,
				height: 40
			}
		}
	]
}