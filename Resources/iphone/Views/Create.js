exports = {
	outlet: "me",
	style: {
		className: "Ti.UI.ScrollView",
		layout: "vertical"
	},
	subviews: [
		{
			outlet: "photoBtn",
			style: {
				className: "Ti.UI.View",
				backgroundImage: "%ResourcesPath%Media/Photo.Panel.png",
				top: 10,				
				width: 125,
				height: 125
			}
		},
		{
			outlet: "nameField",
			style: {
				className: "Ti.UI.TextField",
				borderStyle: Ti.UI.INPUT_BORDERSTYLE_ROUNDED,
  				color: '#336699',
  				hintText: "Name",
  				top: 10, 
  				left: 10,
  				right: 10,
  				width: Ti.UI.FILL,
  				height: 40
			}
		},
		{
			outlet: "markField",
			style: {
				className: "Ti.UI.TextField",
				borderStyle: Ti.UI.INPUT_BORDERSTYLE_ROUNDED,
  				color: '#336699',
  				hintText: "Mark",
  				top: 10, 
  				left: 10,
  				right: 10,
  				width: Ti.UI.FILL,
  				height: 40
			}
		},
		{
			outlet: "placeField",
			style: {
				className: "Ti.UI.TextField",
				borderStyle: Ti.UI.INPUT_BORDERSTYLE_ROUNDED,
  				color: '#336699',
  				hintText: "Place",
  				top: 10, 
  				left: 10,
  				right: 10,
  				width: Ti.UI.FILL,
  				height: 40
			}
		},
		{
			outlet: "commentField",
			style: {
				className: "Ti.UI.TextField",
				borderStyle: Ti.UI.INPUT_BORDERSTYLE_ROUNDED,
  				color: '#336699',
  				hintText: "Comment",
  				top: 10, 
  				left: 10,
  				right: 10,
  				width: Ti.UI.FILL,
  				height: 40
			}
		},
		{
			outlet: "createBtn",
			style: {
				className: "Ti.UI.Button",
				systemButton: Ti.UI.iPhone.SystemButton.DONE,				
				title: "Create",
				top: 30,
				left: 30,
				right: 30,
				width: Ti.UI.FILL,
				height: 40
			}
		}
	]
}