var TiTools = require("TiTools/TiTools");

//---------------------------------------------//

// window container
var indWin = Ti.UI.createWindow();

// black view
var indView = Ti.UI.createView({
	height: Ti.UI.SIZE,
	width: Ti.Platform.displayCaps.platformWidth - 50,
	backgroundColor: '#000',
	borderRadius: 10,
	opacity: 0.8,
	layout: "vertical"
});

// loading indicator	
var actInd = Ti.UI.createActivityIndicator({
	style: Ti.UI.iPhone && Ti.UI.iPhone.ActivityIndicatorStyle.BIG,
	top: 25,
	height: 30,
	width: 30
});
indView.add(actInd);

// message			
indView.add(Ti.UI.createLabel({
	text: "Подождите...",
	color: '#fff',
	font: {fontSize: 20, fontWeight: 'bold'},
	top: 25
}));

// empty view
indView.add(Ti.UI.createView({
	height: 20	
}));

// add black container view
indWin.add(indView);

// close event handler
indWin.addEventListener('close', function()
{
	indView = null;
	actInd  = null;
});

actInd.show();


function show(msg)
{
	//var message = msg || 'Подождите...';
	indWin.open();	
}

function hide()
{
	indWin.close({
		opacity: 0,
		duration: 500
	});
}


module.exports = {
	show: show,
	hide: hide
}
